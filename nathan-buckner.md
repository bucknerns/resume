# Nathan Buckner

> bucknerns@gmail.com • (210) 396-8151

----

>  Detail-oriented, highly motivated Senior Software Engineer with a passion for learning and making an impact.
>  Exceptional design, debugging, testing, problem solving and critical thinking.
>  Searching for a company that I can be an integral part, culturally and intellectually.

----

### Work Experience

**Roku** - Senior Software Engineer - **Jun 2018 - Current**

* Worked as a technical lead for the team
* Wrote internal event collection, payment provideo event collection services
* Created EMR pipelines for transforming and publish service events to data warehouse
* Migrated PCI data to new payment provider
* Wrote, packaged and setup CI pipelines for testing framework

**Amazon Lab126** - Senior Software Developer L5,L6(**promoted**) - **May 2017 - Jun 2018**

* Designed and implemented a test framework to replace organically grown FireTV test framework
* Designed page object model navigation library for FireTV testing
* Conducted training effort for local and overseas employees in person

**Rackspace** - Software Developer/Security Developer in Test II-IV - **Mar 2012 - Feb 2017**

* Built clients and behaviors to facilitate Rackspace to AWS migrations
* Developed complex deployments and test scenarios for migrations
* Created metrics API backed by object storage with ELK stack dashboard
* Designed test harness for software defined networks
* Ran onboarding class for new hires
* Capacity dashboard (Flask/Angular)
* Billing Testing Automation
* Threat Models, Code reviews, infrastructure/web app pen-testing

**General Dynamics Mission Systems** - Senior Engineer - **May 2009 - Jun 2012**

* Embedded radio development
* Kernel, posix and application layers
* RIPV2 development
* Secret Clearance
* With internship in 2009

**US Army** - Signal Support System Specialist - **May 2000 - Jun 2006**

* Secret Clearance, comsec custodian
* Cisco Router configurations and maintenance
* Networking certs - Network+, CCNA
* Windows System Administration
* Tactical System Administration (Force XXI Battle Command Brigade and Below)

### Technical Specifications

**Languages** - Python, Java, Shell/Bash

**Databases** - DynamoDB, Redis, SQL databases + ORMs, MongoDB

**Web** - Spring Boot, Flask, Falcon, Requests, Protobuf, Fastapi, Json, Avro

**Cloud/Virtualization** - AWS, Openstack, Docker, Terraform, ECS, EC2

**CI/CD+Tools** - Gitlabci, Jenkins, Git, Tox, Unittest, Checkstyle, Junit, Gerrit, Gradle, Pip

### Education

**University of Texas at San Antonio** - Bachelor's in Computer Science - **2006 - 2010**

**Certs** - CCNA, Network+

### Links

**Github** - https://github.com/bucknerns

**LinkedIn** - https://www.linkedin.com/in/nathanbuckner
